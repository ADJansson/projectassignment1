#include <iostream>
#include "adjlib/list.h"

/**
 * @brief main
 * A small program to test some of the functions in MyList
 * @return
 */
int main()
{

    // Fill with data using initialzer list
    adjlib::MyList myList = {2, 5, 6, 3, 7, 13};
    // Check the copy constructor
    adjlib::MyList myListCopy(myList);
    // Clear original list
    myList.clear();
    // Print size
    std::cout << "size(): " << myListCopy.size() << "\n";
    // Push a new item to front
    std::cout << "push_front(4)\n";
    myListCopy.push_front(4);
    // Get size of list
    uintptr_t size = myListCopy.size();
    std::cout << "size after push: " << size << "\n";
    // Traverse and pop last
    for (uintptr_t i = 0; i < size; i ++) {
        std::cout << "popping " << i << " : " << myListCopy.pop_back() << "\n";
    }

    // Check if _first element has been set correctly after pop
    std::cout << "first element is " << myListCopy.front() << "\n";

    // Check if size has been updated correctly
    std::cout << "size after pop: " << myListCopy.size() << "\n";

    // Check if list is empty
    std::cout << "is list empty after pop? " << (myListCopy.empty() ? "yes" : "no") << "\n";
    return 0;
}

