#ifndef LIST_H
#define LIST_H

#include <initializer_list>
#include <iterator>
#include <memory>
#include "element.h"

namespace adjlib {
    class MyList {
    using iterator = std::shared_ptr<Element>;
    using size_type = std::size_t;
    using value_type = int;
    private:
        size_type _size = 0;
        iterator _firstElement;
        iterator _lastElement;

    public:
        // Constructors
        MyList (std::initializer_list<value_type> list);
        ~MyList();
        // http://www.cplusplus.com/articles/y8hv0pDG/ 09/09-15 12:30
        MyList (MyList& listCopy);
        MyList& operator=(MyList& moveList);

        // Iterators
        iterator begin();
        iterator end();
        iterator cbegin() const;
        iterator cend() const;

        // Capacity
        size_type size() const;
        bool empty() const;

        // Element access
        value_type front() const;
        value_type back() const;

        // Modifiers
        void erase(iterator element);
        void clear();
        void push_back(value_type value);
        void push_front(value_type value);
        value_type pop_back();

    };

} // END namespace adjlib


#endif // LIST_H

