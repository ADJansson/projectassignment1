#include "list.h"
#include "element.h"

namespace adjlib {

// Constructors
MyList::MyList(std::initializer_list<MyList::value_type> list){

    _firstElement = nullptr;
    _lastElement = nullptr;

    for (auto i : list) {
        push_back(i);
    }
}

MyList::~MyList() {

    _firstElement.reset();
    _lastElement.reset();
    _size = 0;
}

MyList::MyList(MyList &listCopy) {

    MyList::iterator current = listCopy.begin();

    push_back(current->getValue());

    while (current->getNext()) {
        current = current->getNext();
        push_back(current->getValue());
    }

}

// This is supposed to assign a new MyList in place of the old,
// not working due to an error with references
MyList& MyList::operator =(MyList& movelist) {

    // Check if the two instances are the same object
    if (&movelist != this) {

        MyList::iterator current = movelist.begin();

        push_back(current->getValue());

        while (current->getNext()) {
            current = current->getNext();
            push_back(current->getValue());
        }

        // Clears the old list
        movelist.clear();
    }

    return *this;
}

// Iterators
MyList::iterator MyList::begin() {
    return _firstElement;
}

MyList::iterator MyList::end() {
    return _lastElement;
}

MyList::iterator MyList::cbegin() const {
    return _firstElement;
}

MyList::iterator MyList::cend() const {
    return _lastElement;
}

// Capacity
MyList::size_type MyList::size() const {
    return _size;
}

bool MyList::empty() const {
    return _size == 0;
}

// Element access
MyList::value_type MyList::back() const {
    // Should do something better than just returning 0
    return _lastElement ?
                _lastElement->getValue() : 0;
}

MyList::value_type MyList::front() const {
    return _firstElement ?
                _firstElement->getValue() : 0;
}


// Modifiers
void MyList::erase(MyList::iterator element) {

}

void MyList::clear() {
    _firstElement.reset();
    _size = 0;
}

void MyList::push_back(MyList::value_type value) {

    // Creates new element
    std::shared_ptr<Element> element =
        std::make_shared<Element>(value);

    // Checks corner case
    if (!_firstElement) {
        _firstElement = element;
        _lastElement = element;
    }
    // Inserts new element at back
    else {
        _lastElement->setNext(element);
        _lastElement = element;
    }
    _size++;

}

void MyList::push_front(MyList::value_type value) {

    // Make new element
    std::shared_ptr<Element> newElement =
        std::make_shared<Element>(value);

    // Handle corner case
    if (empty()) {
        _firstElement = newElement;
    }
    else {
        // Set new's next to the first element
        newElement->setNext(_firstElement);
        // Update first element
        _firstElement = newElement;
    }

    _size++;
}

MyList::value_type MyList::pop_back() {
    // Should do something better than just returning 0
    if (!_lastElement)
        return 0;

    MyList::iterator current = _firstElement;

    // Find second to last element
    while (current->getNext() && current->getNext() != _lastElement) {
        current = current->getNext();
    }

    // Get value to be returned
    MyList::value_type value = _lastElement->getValue();
    current->setNext(nullptr);

    _lastElement = current;

    _size--;

    // Handle last element
    if (empty())
        _firstElement.reset();

    return value;
}

} // END namespace adjlib
