#ifndef ELEMENT_H
#define ELEMENT_H
#include <memory>
#include "list.h"

// tips for http://stackoverflow.com/questions/2706129/can-a-c-class-include-itself-as-an-attribute 25/8-15

namespace adjlib {
    class Element {
        using value_type = int;
        //TODO change to generic
        value_type _value;
        std::shared_ptr<Element> _next;

    public:
        Element () {
            _value = 0;
            _next = nullptr;
        }

        Element(value_type value) {
            _value = value;
            _next = nullptr;
        }

        Element(value_type value, std::shared_ptr<Element> next) {
            _value = value;
            _next = next;
        }

        ~Element() {
            _value = 0;
            _next.reset();
        }

        std::shared_ptr<Element> getNext() {
            return _next;
        }

        void setNext(std::shared_ptr<Element> next) {
            _next = next;
        }

        void setValue(value_type value) {
            _value = value;
        }

        value_type getValue() {
            return _value;
        }
    };
}


#endif // ELEMENT_H

